package com.mygdx.mypaint;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

public class Image {

	int width;
	int height;
	Texture texture;

	public Image(int width, int height, float r, float g, float b, float a) {

		this.width = width;
		this.height = height;

		Pixmap newImage = new Pixmap(width, height, Pixmap.Format.RGBA8888);
		newImage.setColor(r, g, b, a);
		newImage.fill();

		texture = new Texture(newImage);

		newImage.dispose();


	}

	public void dispose() {
		texture.dispose();
		texture = null;
	}

}
