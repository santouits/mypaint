package com.mygdx.mypaint;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyPaint extends ApplicationAdapter {
	SpriteBatch batch;
	Image img;
	Brush brush;
	CanvasInput canvasInput;
	State state;
	
	@Override
	public void create () {

		batch = new SpriteBatch();
		
		state = new State();

		canvasInput = new CanvasInput();
		canvasInput.state = state;

		brush = new Brush();
//		brush.setSize(10);


		state.tool = brush;
		
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		img = new Image(width, height, 1f, 1f, 1f, 1f);

		state.image = img;

		Gdx.input.setInputProcessor(canvasInput);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img.texture, 0, 0);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		brush.dispose();
	}
}
