package com.mygdx.mypaint;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap;

import java.lang.Math;

public class Brush extends Tool {
	Pixmap pixmap;
	int size;
	float r, g, b, a;

	int previousX;
	int previousY;

	public Brush() {

		size = 1;
		r = 0f;
		g = 0f;
		b = 0f;
		a = 1f;
		
		recreatePixmap();
	}

	public void setSize(int size) {
		if (this.size == size) {
			return;
		}

		this.size = size;

		recreatePixmap();
		
	}

	public void setColor(float r, float g, float b, float a) {
		if (this.r == r && this.g == g && this.b == b && this.a == a) {
			return;
		}

		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;

		pixmap.setColor(r, g, b , a);
		pixmap.fill();
	}

	public void recreatePixmap() {
		if (pixmap != null) {
			pixmap.dispose();
		}

		pixmap = new Pixmap(size, size, Pixmap.Format.RGBA8888);
		pixmap.setColor(r, g, b, a);
		pixmap.fill();
	}

	public void dispose() {
		pixmap.dispose();
	}

	public void use() {

	}

	public void select() {

	}

	public void touched(int x, int y, State state) {
		System.out.println("brush touched: " + x + ", " + y);
		Texture texture = state.image.texture;
		texture.draw(pixmap, x, y);

		previousX = x;
		previousY = y;
	}

	public void dragged(int x, int y, State state) {
		// TODO understand Bresenham's_line_algorithm

		System.out.println("brush touched: " + x + ", " + y);

		Texture texture = state.image.texture;

		int absX = Math.abs(x - previousX);
		int absY = Math.abs(y - previousY);

		if (absX > 1 || absY > 1) {
			Pixmap pm = new Pixmap(absX + 1, absY + 1, Pixmap.Format.RGBA8888);
			pm.setColor(r, g, b, 0f);
			pm.fill();
			pm.setColor(r, g, b, a);

			int pixmap_pos_x;
			int pixmap_pos_y;
			int startX;
			int endX;
			int startY;
			int endY;

			if (previousX < x) {
				pixmap_pos_x = previousX;
				startX = 0;
				endX = absX;
			} else {
				pixmap_pos_x = x;
				startX = absX;
				endX = 0;
			}

			if (previousY < y) {
				pixmap_pos_y = previousY;
				startY = 0;
				endY = absY;
			} else {
				pixmap_pos_y = y;
				startY = absY;
				endY = 0;
			}

			pm.drawLine(startX, startY, endX, endY);

			texture.draw(pm, pixmap_pos_x, pixmap_pos_y);

			pm.dispose();

			

		}

		texture.draw(pixmap, x, y);

		previousX = x;
		previousY = y;
		
	}
}

